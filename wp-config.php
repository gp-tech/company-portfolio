<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_golden_new' );

/** MySQL database username */
define( 'DB_USER', 'goldenphoenix' );

/** MySQL database password */
define( 'DB_PASSWORD', 'P@s$w0rd123!' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );


define( 'WP_HOME', 'https://goldenphoenixvn.com' );
define( 'WP_SITEURL', 'https://goldenphoenixvn.com' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@tlGi?$ype!AHWV5~lU.t5D:o~Db?c0mis;a*)S@%T}v*^xS D=J;VLJ[$m-T=}C' );
define( 'SECURE_AUTH_KEY',  'kGs@#n_KaAY/ 2VThR1eZ0H!CkOV<u&ka.BXZ!NFFR$*j)]i)yc}pPbayTMVTZ9 ' );
define( 'LOGGED_IN_KEY',    'nZ$uhRn_7ogF-YY~;]cQEVZ{sAE7<Z#m%AXB@/dXcjkqr5;l2/+h(9x=Am>z6S=~' );
define( 'NONCE_KEY',        'G-E9Q3sC7P@PE6$n;iaF-fcS6ID%iG1blQ<W@.Oi>CpGy}hB0)78gB*HSg%gZXjM' );
define( 'AUTH_SALT',        '/]TGaSNBWp>19DvZxn hSWHlM?x^W3xyZuxw=bg-i9CUJLq$6xq-l/QoR]vcL@mg' );
define( 'SECURE_AUTH_SALT', 'Xlk,u}vv1OaR_YAE,C9tjD}}BC7,RVW1{0PpR](yFmOF&afzHVY/`l2MVV.u*1](' );
define( 'LOGGED_IN_SALT',   '$^glp]gdNQH-hmZC.Tfm{k{_}#tkM!nJgR2zFc0vhcqJc|}AF%3A[pzK}W;WEyJ9' );
define( 'NONCE_SALT',       'F3ck|eY3Nv2SJX6Zciput9;{FglG,>$6d+&X({G-qOs#RLkDK%Wo+Ur|.q*ftDrk' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
